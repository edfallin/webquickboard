/**/

export class IO {
    constructor() {
        /* No operations. */
    }

    async writeToClipboard(text) {
        await navigator.clipboard.writeText(text);
    }

    async readFromClipboard(to) {
        let text = await navigator.clipboard.readText();
        to.value = text;
    }
}
