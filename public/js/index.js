/**/

import {Blurb} from "./blurb.js";
import {BlurbSet} from "./blurbset.js";
import {DomList} from "./domlist.js";
import { DomBlurbSet } from "./domblurbset.js";
import {IO} from "./io.js";

export class IndexController {
    constructor(dom) {
        this.dom = dom;
        this.blurbSets = [];
        this.io = new IO();

        // Used to give all blurb-set and blurb subtrees individual
        // IDs so they can be dragged and dropped accurately.
        this.idIndex = 0;
    }

    init() {
        this.retrieveAnyBlurbContent();
        this.convertBlurbSetsToDomBlurbSets();
        this.displayDomBlurbSets();
    }

    retrieveAnyBlurbContent() {
        let blurbSetCount = localStorage.length;

        for (let at = 0; at < blurbSetCount; at++) {
            let setName = localStorage.key(at);
            let asJson = localStorage.getItem(setName);

            let blurbSet = BlurbSet.fromJson(asJson);
            this.blurbSets.push(blurbSet);
        }
    }

    convertBlurbSetsToDomBlurbSets() {
        /* Future: Loop using constructors for DomBlurbSets, added to a new DomList. */

        /* Future: Then add the empty blurb set and its eventing,
           unless that would work better in display__(). */
    }

    /* Future: Call displayList() on the DomList of DomBlurbSets,
       with creator() possibly including displaying the actual blurbs,
       and maybe keeping this named method for that, maybe not. */
    displayDomBlurbSets() {
    }

    // region Delegates for blurb DomList objects

    blurbAbstractor(node) {
        // Getting the text from a Text somewhere in a BlurbContent.
        let textEl = node.querySelector("#BlurbContent #Text");
        let text = textEl.value;

        // Actually converting to a blurb.
        let blurb = new Blurb(text, null);
        return blurb;
    }

    blurbMatcher(left, right) {
        /* Only matching on text contents, for now at least. */
        return left.text === right.text;
    }

    blurbCreator(blurb) {
        // New DOM subtree and its unique ID.
        let stencil = this.dom.blurbStencil;
        let node = stencil.cloneNode(true);
        node.id = this.idIndex++;

        // Setting the blurb's text.
        let text = node.querySelector("#BlurbContent #Text");
        text.value = blurb.text;

        // Setting the blurb's level style.
        let blob = node.querySelector("#BlurbContent");
        blob.classList.add(blurb.level);

        // Setting the blurb's eventing.
        this.addBlurbEventListeners(node, this);

        return node;
    }

    blurbStarter(stencil) {
        // Getting stencil's text contents.
        let textEl = stencil.querySelector(".inner-blurb #Text");
        let text = textEl.value;

        return text;
    }

    // endregion Delegates for blurb DomList objects


    // region Adding event listeners

    addBlurbSetEventListeners(node, self) {
        /* Instance passed as "self" since this is called
           using delegation syntax and :. is denatured. */

        node.addEventListener("dragstart", (e) => {
            self.atDragStart(e);
        });
        node.addEventListener("dragover", (e) => {
            self.atDragOver(e);
        });
        node.addEventListener("dragleave", (e) => {
            self.atDragLeave(e);
        });
        node.addEventListener("drop", (e) => {
            self.atBlurbSetDrop(e);
        });

        let dumpButton = node.querySelector("#DumpSetButton");

        dumpButton.addEventListener("click", (e) => {
            self.atDumpSetButtonClick(e);
        });
    }

    addBlurbEventListeners(node, self) {
        /* Instance passed as "self" since this is called
           using delegation syntax and :. is denatured. */

        node.addEventListener("dragstart", (e) => {
            self.atDragStart(e);
        });
        node.addEventListener("dragover", (e) => {
            self.atDragOver(e);
        });
        node.addEventListener("dragleave", (e) => {
            self.atDragLeave(e);
        });

        node.addEventListener("drop", (e) => {
            self.atBlurbDrop(e);
        });
        node.addEventListener("click", (e) => {
            self.atBlurbClick(e);
        });

        let dumpButton = node.querySelector("#DumpBlurbButton");
        dumpButton.addEventListener("click", (e) => {
            self.atDumpBlurbButtonClick(e);
        });

        let useButton = node.querySelector("#UseBlurbButton");
        useButton.addEventListener("click", (e) => {
            self.atCopyButtonClick(e);
        });
    }

    addEmptyBlurbSetEventListeners(node, self) {
        node.addEventListener("dragover", (e) => {
            self.atDragOver(e);
        });
        node.addEventListener("dragleave", (e) => {
            self.atDragLeave(e);
        });
        node.addEventListener("drop", (e) => {
            self.atBlurbSetDrop(e);
        });
    }

    addEmptyBlurbEventListeners(node, self) {
        node.addEventListener("dragover", (e) => {
            self.atDragOver(e);
        });
        node.addEventListener("dragleave", (e) => {
            self.atDragLeave(e);
        });
        node.addEventListener("drop", (e) => {
            self.atBlurbDrop(e);
        });

        let addButton = node.querySelector("#AddBlurbButton");
        addButton.addEventListener("click", (e) => {
            self.addNewBlurb(e);
        });
    }

    // endregion Adding event listeners


    // region Drag and drop eventing

    atDragStart(e) {
        let id = e.target.id;
        e.dataTransfer.setData("text/plain", id);
        e.dataTransfer.dropEffect = "move";
    }

    atDragOver(e) {
        e.preventDefault();
        let over = e.currentTarget;
        over.classList.add("over");
    }

    atDragLeave(e) {
        e.preventDefault();
        let over = e.currentTarget;
        over.classList.remove("over");
    }

    atBlurbSetDrop(e) {
        e.preventDefault();

        // Identifying draggee and droppee.
        let id = e.dataTransfer.getData("text/plain");
        let dragged = this.dom.doc.getElementById(id);

        let onto = e.currentTarget;
        onto.classList.remove("over");

        // Moving the item onscreen.
        this.dom.blurbSets.removeChild(dragged);
        this.dom.blurbSets.insertBefore(dragged, onto);

        // Splicing out from the drag point.
        let source = this.blurbSets;

        // Getting identifiers to find splice indices.
        let fromText = this.textFromEl(dragged);
        let toText = this.textFromEl(onto);

        // Getting the moved blurb set itself.
        let draggedBlurbSet = this.setFromSetName(fromText);

        // Splicing out.
        let from = source.findIndex((x) => x.text === fromText);
        source.splice(from, 1);

        // Splicing in, after splice-out so indices are right.
        let to = source.findIndex((x) => x.text === toText);
        to = to !== -1 ? to : source.length;

        source.splice(to, 0, draggedBlurbSet);
    }

    atBlurbDrop(e) {
        e.preventDefault();

        // Identifying draggee and droppee.
        let id = e.dataTransfer.getData("text/plain");
        let dragged = this.dom.doc.getElementById(id);

        let onto = e.currentTarget;
        onto.classList.remove("over");

        // Getting parents, for visible dnd and internal changing.
        let parentFrom = dragged.parentNode;
        let parentOnto = onto.parentNode;

        // Moving the item onscreen.
        parentFrom.removeChild(dragged);
        parentOnto.insertBefore(dragged, onto);

        // Getting names of blurb sets to then find them.
        let fromName = this.setNameFromBlurbParent(parentFrom);
        let toName = this.setNameFromBlurbParent(parentOnto);

        // Getting the blurb sets, then their blurbs.
        let from = this.setFromSetName(fromName);
        let to = this.setFromSetName(toName);

        // Getting identifiers to find splice indices.
        let fromText = this.textFromEl(dragged);
        let toText = this.textFromEl(onto);

        // Getting the actual blurbs to work with.
        let draggedBlurb = from.blurbFromText(fromText);
        let ontoBlurb = to.blurbFromText(toText);

        // Splicing out from the drag point.
        from.dumpBlurb(draggedBlurb);

        // Splicing into the drop point.
        // Has to be calculated after splice-out,
        // or the index for splice-in may be wrong.
        to.addBlurb(draggedBlurb, ontoBlurb);

        // Saving changes to storage.
        from.save();
        to.save();
    }

    // endregion Drag and drop eventing


    // region Copy eventing and dependencies

    atBlurbClick(e) {
        let target = e.target;

        // If a child el with another purpose was clicked, ignore this click.
        if (!this.elHasAnyOfTheseClasses(target, "clickable", "inner-blurb")) {
            e.stopPropagation();
            return;
        }

        this.copyFromBlurbText(target);
    }

    atCopyButtonClick(e) {
        let target = e.target;
        this.copyFromBlurbText(target);
    }

    copyFromBlurbText(el) {
        let blurbEl = this.blurbElFromEl(el);
        let text = this.textFromEl(blurbEl);

        this.io.writeToClipboard(text)
            .then(() => {
                    this.flashBlurbEl(blurbEl);
                }
            );
    }

    flashBlurbEl(blurbEl) {
        let target = blurbEl.querySelector(".inner-blurb");
        let text = target.querySelector("#Text");
        target.classList.add("flash");
        text.classList.add("flash");
        setTimeout(() => {
                target.classList.remove("flash");
                text.classList.remove("flash");
            }, 250
        );
    }

    // endregion Copy eventing and dependencies


    // region Add eventing

    addNewBlurb(e) {
        // Getting blurb contents.
        let target = e.target;
        let blurbEl = this.ancestorFromElAndClass(target, "inner-blurb");
        let text = this.textFromEl(blurbEl);

        // Getting blurb set.
        let region = this.ancestorFromElAndClass(target, "region");
        let blurbSet = this.setFromBlurbParent(region);

        // Actually adding blurb.
        let blurb = new Blurb(text, "plain");
        blurbSet.blurbs.push(blurb);

        // Saving the blurb to storage.
        blurbSet.save();

        // Removing just the set's blurbs.
        let blurbNodes = region.querySelectorAll(".blurb");

        for (let blurbNode of blurbNodes) {
            region.removeChild(blurbNode);
        }

        // Redrawing just the set's blurbs.
        this.displayBlurbs(blurbSet.blurbs, region);
        this.displayEmptyBlurb(region);
    }

    // endregion Add eventing


    // region Delete eventing

    atDumpSetButtonClick(e) {
        // Identifying blurb set.
        let target = e.target;
        let setEl = this.ancestorFromElAndClass(target, "blurb-set");
        let setName = this.textFromEl(setEl);

        let source = this.blurbSets;

        // Finding the blurb set and its index in the array.
        let from = source.findIndex((x) => x.text === setName);
        let blurbSet = source[from];

        // Dumping the blurb set from array.
        source.splice(from, 1);

        // Removing the blurb set from the UI.
        this.dom.blurbSets.removeChild(setEl);

        // Removing the blurb set's pane from the UI.
        let regions = this.dom.blurbs
            .querySelectorAll(".region");

        for (let region of regions) {
            let regionName = this.nameFromEl(region);

            if (regionName === setName) {
                this.dom.blurbs.removeChild(region);
                break;
            }
        }

        // Removing the blurb set from local storage.
        blurbSet.dump();
    }

    atDumpBlurbButtonClick(e) {
        let target = e.target;

        let blurbEl = this.ancestorFromElAndClass(target, "blurb");
        let region = this.ancestorFromElAndClass(blurbEl, "region");

        // Drop from the blurb set.
        let blurbSet = this.setFromBlurbParent(region);
        let text = this.textFromEl(blurbEl)
        let blurb = blurbSet.blurbFromText(text);
        blurbSet.dumpBlurb(blurb);

        // Drop from the stored blurb set.
        blurbSet.save();

        // Drop from the UI.
        region.removeChild(blurbEl);
    }

    // endregion Delete eventing


    blurbElFromEl(el) {
        let blurbEl = this.ancestorFromElAndClass(el, "blurb");
        return blurbEl;
    }

    setFromBlurbParent(parent) {
        let name = this.setNameFromBlurbParent(parent);
        let set = this.setFromSetName(name);
        return set;
    }

    setNameFromBlurbParent(parent) {
        let nameEl = parent.querySelector("#Name");
        let name = nameEl.innerText;
        return name;
    }

    setFromSetName(name) {
        let set = this.blurbSets
            .find((x) => x.text === name);
        return set;
    }

    textFromEl(el) {
        /* The textEl here is assumed to be an Input
           or TextArea with a .value attribute. */

        let textEl = el.querySelector("#Text");
        let text = textEl.value;
        return text;
    }

    nameFromEl(el) {
        /* The nameEl here is assumed to be a Div or other
           non-input node with an .innerText attribute. */

        let nameEl = el.querySelector("#Name");
        let name = nameEl.innerText;
        return name;
    }

    ancestorFromElAndClass(child, name) {
        let node = child.parentNode;

        while (!node.classList.contains(name)) {
            node = node.parentNode;
        }

        return node;
    }

    elHasAnyOfTheseClasses(el, ...names) {
        for (let name of names) {
            if (el.classList.contains(name)) {
                return true;
            }
        }

        return false;
    }

    saveAllBlurbSets() {
        // Saving all current blurb sets.
        for (let blurbSet of this.blurbSets) {
            blurbSet.save();
        }
    }
}
