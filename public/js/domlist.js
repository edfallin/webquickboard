/**/

export class DomList {
    /* Supports adding and subtracting existing items, and adding
       all-new ones as well.  Doesn't handle persistent storage. */

    static get notPresent() { return -1; }

    // "Items" is an array containing the items in the list.
    // "Root" is the DOM node containing the list, normally by itself.
    // "Abstractor" is code that extracts an item from its DOM representation.
    // "Matcher" is code that identifies two items as the same.
    // "Creator" is code that turns an instance into a DOM node.
    // "Start" is a stencil DOM node that can be turned into an item by starter.
    // "Starter" is code that turns a filled stencil DOM node into a list item.
    constructor(items, root, abstractor, matcher, creator, start, starter) /* passed */ {
        // Default start items array if none provided.
        if (!items) {
            items = [];
        }

        // From args to instance members.
        this.items = items;
        this.root = root;
        this.abstractor = abstractor;
        this.matcher = matcher;
        this.creator = creator;
        this.start = start;
        this.starter = starter;
    }

    displayList() /* passed */ {
        // Creating all the items to be displayed at once.
        let domItems = [];

        for (let item of this.items) {
            let domItem = this.creator(item);
            domItems.push(domItem);
        }

        // Removing the entire existing list's DOM representation.
        while (this.root.children.length > 0) {
            this.root.removeChild(this.root.lastChild);
        }

        // Actually displaying the list again.
        // Done separately and later than creation for faster redisplay.
        for (let domItem of domItems) {
            this.root.appendChild(domItem);
        }
    }

    // region items

    getItem(index) /* passed */ {
        // Edge paths.
        if (index < 0 || index >= this.items.length) {
            return null;
        }

        // Main path.
        return this.items[index];
    }

    doesContainItem(item) /* passed */ {
        let index = this.getItemIndex(item);
        return index != DomList.notPresent;
    }

    getItemIndex(item) /* passed */ {
        let index = this.items.findIndex(x => this.matcher(x, item));
        return index;
    }

    addItem(item, atItem) /* passed */ {
        let index = this.getItemIndex(atItem);

        // Actually adding.
        this.items.splice(index, 0, item);

        this.displayList();
    }

    subtractItem(item) /* passed */ {
        let index = this.getItemIndex(item);

        // Actually subtracting.
        this.items.splice(index, 1);

        this.displayList();
    }

    // endregion items

    // region nodes

    getNode(index) /* passed */ {
        let item = this.getItem(index);

        // Edge path.
        if (item === null) {
            return null;
        }

        // Main path.
        let domEl = this.creator(item);
        return domEl;
    }

    doesContainNode(node) /* passed */ {
        let item = this.getNodeIndex(node);
        return item !== DomList.notPresent;
    }

    getNodeIndex(node) /* passed */ {
        let item = this.abstractor(node);

        let index = this.items
            .findIndex(
                x => this.matcher(x, item)
            );

        return index;
    }

    addNodeItem(node, atNode) /* passed */ {
        let item = this.abstractor(node);
        let index = this.getNodeIndex(atNode);

        // Actually adding.
        this.items.splice(index, 0, item);

        this.displayList();
    }

    subtractNodeItem(node) /* passed */ {
        let index = this.getNodeIndex(node);

        // Actually subtracting.
        this.items.splice(index, 1);

        this.displayList();
    }

    createNodeItemFromStencil() /* passed */ {
        // Actually creating from the stencil node.
        let created = this.starter(this.start);

        // Adding the created item at the end.
        this.items.push(created);

        // Displaying changed list.
        this.displayList();
    }

    // endregion nodes

    // region mixed

    addItemAtNode(item, atNode) /* passed */ {
        let atIndex = this.getNodeIndex(atNode);

        // Actually adding.
        this.items.splice(atIndex, 0, item);

        this.displayList();
    }

    // endregion mixed
}
