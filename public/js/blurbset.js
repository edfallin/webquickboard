/**/

export class BlurbSet {
    constructor(name, blurbs) {
        this.text = name;
        this.blurbs = blurbs;
        this.isFocused = true;
    }

    static fromJson(json) {
        let plain = JSON.parse(json);
        let blurbSet = new BlurbSet(plain.text, plain.blurbs);
        return blurbSet;
    }

    blurbFromText(text) {
        let blurb = this.blurbs.find((x) => x.text === text);
        return blurb;
    }

    addBlurb(blurb, at) {
        // Default if no "at", for instance when adding at the end.
        let to = this.blurbs.length;

        // Common case: there is an "at".
        if (at !== undefined) {
            to = this.blurbs.findIndex((x) => x.text === at.text);
        }

        // Edge case: "at" was not found.
        if (to === -1) {
            to = this.blurbs.length;
        }

        // Adding where dropped.
        this.blurbs.splice(to, 0, blurb);
    }

    dumpBlurb(blurb) {
        let from = this.blurbs.findIndex((x) => x.text === blurb.text);
        this.blurbs.splice(from, 1);
    }

    save() {
        let asJson = JSON.stringify(this);
        localStorage.setItem(this.text, asJson);
    }

    dump() {
        localStorage.removeItem(this.text);
    }
}
