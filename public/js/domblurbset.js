/**/

import {BlurbSet} from "./blurbset.js";
import {DomList} from "./domlist.js";

/* A DomBlurbSet encapsulates a BlurbSet for operations on the blurbs,
   a DomList for maintaining the blurbs and their DOM nodes individually,
   and area roots for displaying both the set identifier and its blurbs. */
export class DomBlurbSet {
    constructor(blurbSet,
                dom, caller,
                { setStencil, emptySetStencil, blurbStencil, emptyBlurbStencil }
    ) {
        // Key active members.
        this.blurbSet = blurbSet;
        this.tile = null;
        this.region = null;
        this.domList = null;

        // Objects referencing the DOM and the calling class.
        this.dom = dom;
        this.caller = caller;
    }

    /* Adds a text node in the set area and then,
       if focused, its blurbs in the blurb area. */
    display() {
        this.displayNameTile();

        if (this.blurbSet.isFocused) {
            this.displayInitedDomList();
        }
    }

    displayNameTile() {
        // New tile.
        let tile = this.dom.blurbSetStencil.cloneNode(true);
        tile.id = this.caller.idIndex++;

        // And its text.
        let name = tile.querySelector("#Name");
        name.innerText = this.blurbSet.text;

        // And its event listeners, on the caller.
        this.caller.addBlurbSetEventListeners(tile, this.caller);

        // Retaining reference to tile as member.
        this.tile = tile;
    }

    displayInitedDomList() {
        /* Region must be inited first so DOM els exist for DomList. */

        this.displayBlurbRegion();
        this.initDomList();
        this.domList.displayList();
    }

    displayBlurbRegion() {
        // New region.
        let region = this.dom.blurbRegionStencil.cloneNode(true);
        region.id = this.caller.idIndex++;

        // And its text.
        let name = region.querySelector("#Name");
        name.innerText = this.blurbSet.text;

        // And its empty blurb stencil.
        let stencil = this.dom.newBlurbStencil;
        let empty = stencil.cloneNode(true);
        empty.id = this.caller.idIndex++;

        // And that stencil's eventing.
        this.caller.addEmptyBlurbEventListeners(empty, this.caller);

        // Displaying the stencil in the region.
        region.appendChild(empty);

        // Actually displaying the new region in the R blurbs pane.
        this.dom.blurbs.appendChild(region);

        // Retaining reference to region as member.
        this.region = region;
    }

    initDomList() {
        // Localizing new region's internal nodes to use
        // when initing the blurb-list -managing object.
        let blurbRoot = this.region.querySelector("#ListRoot");
        let blurbStart = this.region.querySelector("#NewBlurbStencil");

        // Initing the object to manage the region's blurbs.
        let blurbList = new DomList(
            this.blurbSet.blurbs, blurbRoot,
            this.caller.blurbAbstractor,
            this.caller.blurbMatcher,
            this.caller.blurbCreator,
            blurbStart,
            this.caller.blurbStarter
        );

        this.domList = blurbList;
    }

}
