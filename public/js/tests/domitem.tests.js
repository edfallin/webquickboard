/**/

import {DomItem} from "../domitem.js";

// region Fixtures

function supplyInitingArgs() {
    // Trivial basic args.
    let item = {value: "item"};
    let site = {value: "site"};

    // Complex object with members that
    // can be used in test conditions.
    let itemer = {
        indexer: {index: 0},
        builder: (item, index) => {
            return {value: item.value, index: index};
        }
    };

    // Back to caller.
    return {item, site, itemer};
}

// endregion Fixtures

describe("DomItem", () => {
    // region constructor()

    it("constructor() sets arg properties to args provided.", /* working */ () => {
        /* Arrange. */
        let {item, site, itemer} = supplyInitingArgs();

        /* Act. */
        let target = new DomItem(item, site, itemer);

        /* Assert. */
        // Equating identities (rather than properties).
        expect(target.item).toBe(item);
        expect(target.site).toBe(site);
        expect(target.itemer).toBe(itemer);
    });

    it("constructor() sets incremented index and calls builder with right args.", /* working */ () => {
        /* Arrange. */
        let {item, site, itemer} = supplyInitingArgs();

        let expected = {value: "item", index: 0};

        /* Act. */
        let target = new DomItem(item, site, itemer);

        /* Assert. */
        // Index is incremented after property is set.
        expect(target.index).toEqual(0);
        expect(target.node).toEqual(expected);
    });

    it("itemer.indexer arg object retains incremented index after constructor call.", /* working */ () => {
        /* Arrange. */
        let {item, site, itemer} = supplyInitingArgs();

        /* Act. */
        let target = new DomItem(item, site, itemer);

        /* Assert. */
        // Index is incremented after property is set.
        expect(itemer.indexer.index).toEqual(1);
    });

    // endregion constructor()

    // region equals()

    it("equals() returns false when indices of two DomItems are different.", /* working */ () => {
        /* Arrange. */
        let {item, site, itemer} = supplyInitingArgs();

        // Consecutive instances have different
        // indices regardless of other contents.
        let target = new DomItem(item, site, itemer);
        let untarget = new DomItem(item, site, itemer);

        /* Act. */
        let actual = target.equals(untarget);

        /* Assert. */
        expect(actual).toBeFalse();
    });

    it("equals() returns true when indices of two DomItems are the same.", /* working */ () => {
        /* Arrange. */
        let {item, site, itemer} = supplyInitingArgs();

        // The same instance also has the same index;
        // the only case in which they should match.
        let target = new DomItem(item, site, itemer);
        let untarget = target;

        /* Act. */
        let actual = target.equals(untarget);

        /* Assert. */
        expect(actual).toBeTrue();
    });

    // endregion equals()

    // region display()

    it("display() adds this.node to the children of this.site", /* working */ () => {
        /* Arrange. */
        let {item, itemer} = supplyInitingArgs();

        let site = document.createElement("div");
        itemer.builder = (item, index) => {
            let node = document.createElement("span");
            node.id = index;
            return node;
        };

        let instance = new DomItem(item, site, itemer);

        /* Act. */
        instance.display();

        /* Assert. */
        expect(instance.site.children.length).toEqual(1);
        expect(instance.site.children[0]).toBe(instance.node);
    });

    // endregion display()

});
