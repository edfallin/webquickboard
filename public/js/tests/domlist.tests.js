/**/

import {DomList} from "../domlist";

// region Fixtures

// region Code that tests use to make item els.

function buildItemEl(item) /* verified */ {
    let root = document.createElement("div");
    root.classList.add("root");

    let button = document.createElement("button");
    button.id = "Act";

    let contents = document.createElement("div");
    contents.id = "Contents";

    let span = document.createElement("span");
    span.id = "text";
    span.innerText = item;

    root.appendChild(button);
    root.appendChild(contents);
    contents.appendChild(span);

    return root;
}

function buildAllItemEls(items) /* verified */ {
    let els = [];

    for (let item of items) {
        let el = buildItemEl(item);
        els.push(el);
    }

    return els;
}

// endregion Code that tests use to make item els.

// region Arg code that DomList uses to convert texts to DOM els.

function itemSpanCreator(item) /* verified */ {
    let child = document.createElement("span");
    child.innerText = item;
    return child;
}

function itemDivCreator(item) /* verified */ {
    let child = document.createElement("div");
    child.innerText = item;
    return child;
}

// endregion Arg code that DomList uses to convert texts to DOM els.

// Arg code that DomList uses to getItem an item from an el.
function elItemAbstractor(el) /* verified */ {
    let span = el.querySelector("#text");
    let item = span.innerText;
    return item;
}

// Arg code that DomList uses to match items.
function simpleMatcher(left, right) /* verified */ {
    return left === right;
}

// Code that tests use as a DomList internal
// to avoid side-effects of tested calls.
function emptyDisplayList() /* verified */ {
    /* No operations. */
}

// Code that tests use to make separate DomList instances.
function buildInstances(items, displayListNonce, length) /* verified */ {
    let instances = [];

    for (let of = 0; of < length; of++) {
        let instance = new DomList([...items], null, elItemAbstractor, simpleMatcher);
        instance.displayList = displayListNonce;
        instances.push(instance);
    }

    return instances;
}

// endregion Fixtures

describe("DomList", () => {
    // region constructor()

    it("constructor() retains all args correctly.", /* working */ () => {
        /* Arrange. */
        let items = "items";
        let root = "root";
        let abstractor = "abstractor";
        let matcher = "matcher";
        let creator = "creator";
        let start = "start";
        let starter = "starter";

        /* Act. */
        let instance = new DomList(
            items, root,
            abstractor,
            matcher, creator,
            start, starter
        );

        /* Assert. */
        expect(instance.items).toEqual(items);
        expect(instance.root).toEqual(root);
        expect(instance.abstractor).toEqual(abstractor);
        expect(instance.matcher).toEqual(matcher);
        expect(instance.creator).toEqual(creator);
        expect(instance.start).toEqual(start);
        expect(instance.starter).toEqual(starter);
    });

    it("constructor() inits a start .items array if none is provided as arg.", /* working */ () => {
        let instance = new DomList();
        expect(instance.items).toEqual([]);
    });

    // endregion constructor()

    // region displayList()

    it("displayList() clears all existing children of .root.", /* working */ () => {
        /* Arrange. */
        let root = document.createElement("div");

        for (let of = 0; of < 3; of++) {
            let child = document.createElement("span");
            root.appendChild(child);
        }

        // Although called in DisplayList(), .creator isn't needed,
        // because it is only used if .items has contents.
        let instance = new DomList([], root);

        /* Act. */
        instance.displayList();

        /* Assert. */
        // No children after exercising code (since .items is empty).
        expect(instance.root.children.length).toEqual(0);
    });

    it("displayList() adds a child to .root for each item in .items.", /* working */ () => {
        /* Arrange. */
        let root = document.createElement("div");

        let items = ["a", "c", "b"];

        let instance = new DomList(
            items, root, null, null,
            itemSpanCreator, null, null
        );

        /* Act. */
        instance.displayList();

        /* Assert. */
        // Right number of children.
        expect(instance.root.children.length).toEqual(items.length);

        // Each child.
        for (let at = 0; at < items.length; at++) {
            // Expected, and extracted actual.
            let expected = items[at];
            let actualEl = root.children[at];
            let actual = actualEl.innerText;

            // Actual assert for each.
            expect(actual).toEqual(expected);
        }
    });

    it("displayList() replaces existing children of .root with new ones from contents of .items.", /* working */ () => {
        let root = document.createElement("div");

        let before = ["a", "b", "c"];
        let after = ["w", "x", "y", "z"];

        for (let item of before) {
            let child = itemDivCreator(item);
            root.appendChild(child);
        }

        let instance = new DomList(after, root, null, null, itemDivCreator);

        /* Pre-test. */
        expect(instance.root.children.length).toEqual(before.length);

        /* Act. */
        instance.displayList();

        /* Assert. */
        // Right number of children.
        expect(instance.root.children.length).toEqual(after.length);

        // Each child is right.
        for (let at = 0; at < after.length; at++) {
            // Expected and extracted actual.
            let expected = after[at];
            let actualEl = instance.root.children[at];
            let actual = actualEl.innerText;

            // Actually assert for each child.
            expect(actual).toEqual(expected);
        }
    });

    // endregion displayList()

    // region items

    // region getItem()

    it("getItem() returns the item at each given index.", /* working */ () => {
        /* Arrange. */
        let items = ["b", "e", "h", "k", "n"];

        let instance = new DomList(items);

        let ats = [3, 2, 4];

        for (let at of ats) {
            /* Act. */
            let actual = instance.getItem(at);

            /* Assert. */
            expect(actual).toEqual(items[at]);
        }
    });

    it("getItem() returns null if the index is out of range.", /* working */ () => {
        /* Arrange. */
        let items = [6, 2, 11, 10];

        // Indices that are all out of range.
        let unders = [-2, -1];
        let overs = [items.length, items.length + 2];
        let outs = [...overs, ...unders];

        let instance = new DomList(items);

        for (let out of outs) {
            /* Act. */
            let actual = instance.getItem(out);

            /* Assert. */
            expect(actual).toBeNull();
        }
    });

    // endregion getItem()

    // region getItemIndex()

    it("getItemIndex() returns the correct .items index for each in-list item.", /* working */ () => {
        /* Arrange. */
        let items = ["first", "two", "three, at 2", "last"];
        let instance = new DomList(items, null, null, simpleMatcher);

        // Non-trivial input ordering.
        let args = [items[2], items[3], items[1], items[0]];

        let expecteds = [2, 3, 1, 0];

        for (let at = 0; at < items.length; at++) {
            // Localizing.
            let arg = args[at];
            let expected = expecteds[at];

            /* Act. */
            let actual = instance.getItemIndex(arg);

            /* Assert. */
            expect(actual).toEqual(expected);
        }
    });

    it("getItemIndex() returns DomList.notPresent for any item that isn't in its list.", /* working */ () => {
        /* Arrange. */
        let items = ["first", "two", "last"];
        let instance = new DomList(items, null, null, simpleMatcher);

        let notAnItem = buildItemEl("absent");

        /* Act. */
        let actual = instance.getItemIndex(notAnItem);

        /* Assert. */
        expect(actual).toEqual(DomList.notPresent);
    });

    // endregion getItemIndex()

    // region doesContainItem()

    it("doesContainItem() returns true for each in-list item.", /* working */ () => {
        /* Arrange. */
        let items = ["first", "two", "three, at 2", "last"];
        let instance = new DomList(items, null, null, simpleMatcher);

        // Non-trivial input ordering.
        let args = [items[3], items[1], items[2], items[0]];

        let expecteds = [3, 1, 2, 0];

        for (let at = 0; at < items.length; at++) {
            // Localizing.
            let arg = args[at];
            let expected = expecteds[at];

            /* Act. */
            let actual = instance.doesContainItem(arg);

            /* Assert. */
            expect(actual).toBeTrue();
        }
    });

    it("doesContainItem() returns false for any item that isn't in its list.", /* working */ () => {
        /* Arrange. */
        let items = ["first", "two", "last"];
        let instance = new DomList(items, null, null, simpleMatcher);

        let notAnItem = "absent";

        /* Act. */
        let actual = instance.doesContainItem(notAnItem);

        /* Assert. */
        expect(actual).toBeFalse();
    });

    // endregion doesContainItem()

    // region addItem()

    it("addItem() adds the provided item at the list index for the at-item provided.", /* working */ () => {
        /* Arrange. */
        let items = ["a", "b", "c", "letter D"];

        // Non-trivial rearrangement for more meaningful testing.
        let ats = [2, 3, 1, 0];
        let reordered = [
            items[ats[0]], items[ats[1]],
            items[ats[2]], items[ats[3]]
        ];

        // Isolated target instances.
        let instances = buildInstances([...items], emptyDisplayList, items.length);

        // Code is exercised on a different instance for each index.
        for (let of = 0; of < instances.length; of++) {
            let instance = instances[of];
            let newItem = `New item ${of}`;

            /* Act. */
            // Adding new item at index from rearranged order.
            instance.addItem(newItem, reordered[of]);
        }

        /* Assert. */
        for (let of = 0; of < instances.length; of++) {
            // Localizing.
            let instance = instances[of];
            let at = ats[of];

            // Expected values increment, but the
            // expected indices are reordered.
            let expected = `New item ${of}`;
            let actual = instance.items[at];

            // Actually asserting.
            // The items list should have been lengthened by 1,
            // and the extra value should be the arg to insert.
            expect(instance.items.length).toEqual(items.length + 1);
            expect(actual).toEqual(expected);
        }
    });

    it("addItem() invokes displayList() after the list is updated.", /* working */ () => {
        /* If displayList() is called, and after the other steps,
           its list length is 1 higher than the original length. */

        /* Arrange. */
        let items = ["a", "b", "c", "d"];

        // Arg elements.
        let atEl = items[2];
        let newEl = "New Item";

        // The target of the tests.
        let instance = new DomList([...items], null, null, simpleMatcher);

        // Defined here because actual is used in nonce lambda.
        let expected = items.length + 1;
        let actual = 0;

        // Target code, overridden to save the test's actual result.
        instance.displayList = () => {
            actual = instance.items.length;
        };

        /* Act. */
        instance.addItem(newEl, atEl);

        /* Assert. */
        expect(actual).toEqual(expected);
    });

    // endregion addItem()

    // region subtractItem()

    it("subtractItem() removes the provided item from the list.", /* working */ () => {
        /* Arrange. */
        let items = ["One", "Two", "Three"];
        let instances = buildInstances([...items], emptyDisplayList, items.length);

        let expecteds = [
            ["Two", "Three"],
            ["One", "Three"],
            ["One", "Two"]
        ];

        // Results from calls against all instances.
        let actuals = [];

        for (let at = 0; at < items.length; at++) {
            // Localizing.
            let item = items[at];
            let instance = instances[at];

            /* Act. */
            instance.subtractItem(item);

            // Retaining in gathered results.
            actuals.push(instance.items);
        }

        /* Assert. */
        for (let of = 0; of < expecteds.length; of++) {
            // Localizing.
            let expected = expecteds[of];
            let actual = actuals[of];

            // Actually asserting.  Equality of arrays.
            expect(actual).toEqual(expected);
        }
    });

    it("subtractItem() invokes displayList() after the list is updated.", /* working */ () => {
        /* Arrange. */
        let items = ["One", "Two", "Three"];
        let instance = new DomList([...items], null, null, simpleMatcher);

        let arg = items[1];

        let expected = items.length - 1;
        let actual = 0;

        // Target code, overridden to save the test's actual result.
        instance.displayList = () => {
            actual = instance.items.length;
        };

        /* Act. */
        instance.subtractItem(arg);

        /* Assert. */
        // Equality of arrays.
        expect(actual).toEqual(expected);
    });

    // endregion subtractItem()

    // endregion items

    // region nodes

    // region getNode()

    it("getNode() returns an el for its item at the index provided.", /* working */ () => {
        /* Arrange. */
        let items = ["2", "4", "7", "8"];

        // Items are tried out of order.
        let indices = [2, 0, 3, 1];
        let mixed = [items[indices[0]], items[indices[1]], items[indices[2]], items[indices[3]]];

        let instance = new DomList(items, null, null, null, itemSpanCreator);

        for (let at = 0; at < indices.length; at++) {
            let expected = mixed[at];

            /* Act. */
            let actualEl = instance.getNode(indices[at]);
            let actual = actualEl.innerText;

            /* Assert. */
            expect(actual).toEqual(expected);
        }
    });

    it("getNode() returns null if the index is out of range.", /* working */ () => {
        /* Arrange. */
        let items = [6, 2, 11, 10];

        // Indices that are all out of range.
        let unders = [-2, -1];
        let overs = [items.length, items.length + 2];
        let outs = [...overs, ...unders];

        let instance = new DomList(items, null, null, null, itemDivCreator);

        for (let out of outs) {
            /* Act. */
            let actual = instance.getNode(out);

            /* Assert. */
            expect(actual).toBeNull();
        }
    });

    // endregion getNode()

    // region getNodeIndex()

    it("getNodeIndex() returns the correct .items index for each in-list item abstracted from an el.", /* working */ () => {
        /* Arrange. */
        let items = ["first", "two", "three, at 2", "last"];

        // Building els to getNodeIndex from items.
        let els = buildAllItemEls(items);

        let instance = new DomList(items, null, elItemAbstractor, simpleMatcher);

        // Non-trivial input ordering.
        els.reverse();

        let expecteds = [3, 2, 1, 0];

        for (let at = 0; at < items.length; at++) {
            // Localizing.
            let el = els[at];
            let expected = expecteds[at];

            /* Act. */
            let actual = instance.getNodeIndex(el);

            /* Assert. */
            expect(actual).toEqual(expected);
        }
    });

    it("getNodeIndex() returns DomList.notPresent for any el-abstracted item that isn't in its list.", /* working */ () => {
        /* Arrange. */
        let items = ["first", "two", "last"];
        let instance = new DomList(items, null, elItemAbstractor, simpleMatcher);

        let notAnItem = buildItemEl("absent");

        /* Act. */
        let actual = instance.getNodeIndex(notAnItem);

        /* Assert. */
        expect(actual).toEqual(DomList.notPresent);
    });

    // endregion getNodeIndex()

    // region doesContainNode()

    it("doesContainNode() returns true for each in-list item extracted from an el.", /* working */ () => {
        /* Arrange. */
        let items = ["first", "two", "three, at 2", "last"];
        let els = buildAllItemEls(items);

        let instance = new DomList(items, null, elItemAbstractor, simpleMatcher);

        // Non-trivial input ordering.
        els.reverse();

        let expecteds = [3, 2, 1, 0];

        for (let at = 0; at < items.length; at++) {
            // Localizing.
            let el = els[at];
            let expected = expecteds[at];

            /* Act. */
            let actual = instance.doesContainNode(el);

            /* Assert. */
            expect(actual).toBeTrue();
        }
    });

    it("doesContainNode() returns false for any el-abstracted item that isn't in its list.", /* working */ () => {
        /* Arrange. */
        let items = ["first", "two", "last"];
        let instance = new DomList(items, null, elItemAbstractor, simpleMatcher);

        let notAnItem = buildItemEl("absent");

        /* Act. */
        let actual = instance.doesContainNode(notAnItem);

        /* Assert. */
        expect(actual).toBeFalse();
    });

    // endregion doesContainNode()

    // region addNodeItem()

    it("addNodeItem() adds the provided abstracted item at the list index for the el provided.", /* working */ () => {
        /* Arrange. */
        let items = ["a", "b", "c", "letter D"];

        // Non-trivial rearrangement for more meaningful testing.
        let ats = [2, 3, 1, 0];
        let reordered = [
            items[ats[0]], items[ats[1]],
            items[ats[2]], items[ats[3]]
        ];
        let els = buildAllItemEls(reordered);

        // Isolated target instances.
        let instances = buildInstances([...items], emptyDisplayList, items.length);

        // Code is exercised on different instance for each index.
        for (let of = 0; of < instances.length; of++) {
            let instance = instances[of];
            let newEl = buildItemEl(`New item ${of}`);

            /* Act. */
            // Adding new el at index from rearranged order.
            instance.addNodeItem(newEl, els[of]);
        }

        /* Assert. */
        for (let of = 0; of < instances.length; of++) {
            // Localizing.
            let instance = instances[of];
            let at = ats[of];

            // Expected values increment, but the
            // expected indices are reordered.
            let expected = `New item ${of}`;
            let actual = instance.items[at];

            // Actually asserting.
            // The items list should have been lengthened by 1,
            // and the extra value should be the arg to insert.
            expect(instance.items.length).toEqual(items.length + 1);
            expect(actual).toEqual(expected);
        }
    });

    it("addNodeItem() invokes displayList() after the list is updated.", /* working */ () => {
        /* If displayList() is called, and after the other steps,
           its list length is 1 higher than the original length. */

        /* Arrange. */
        let items = ["a", "b", "c", "d"];

        // Arg elements.
        let atEl = buildItemEl(items[2]);
        let newEl = buildItemEl("New Item");

        // The target of the tests.
        let instance = new DomList([...items], null, elItemAbstractor, simpleMatcher);

        // Defined here because actual is used in nonce lambda.
        let expected = items.length + 1;
        let actual = 0;

        // Target code, overridden to save the test's actual result.
        instance.displayList = () => {
            actual = instance.items.length;
        };

        /* Act. */
        instance.addNodeItem(newEl, atEl);

        /* Assert. */
        expect(actual).toEqual(expected);
    });

    // endregion addNodeItem()

    // region subtractNodeItem()

    it("subtractNodeItem() removes the provided abstracted item from the list.", /* working */ () => {
        /* Arrange. */
        let items = ["One", "Two", "Three"];
        let els = buildAllItemEls(items);

        let instances = buildInstances([...items], emptyDisplayList, items.length);

        let expecteds = [
            ["Two", "Three"],
            ["One", "Three"],
            ["One", "Two"]
        ];

        let actuals = [];

        for (let at = 0; at < els.length; at++) {
            // Localizing.
            let el = els[at];
            let instance = instances[at];

            /* Act. */
            instance.subtractNodeItem(el);
            actuals.push(instance.items);
        }

        /* Assert. */
        for (let of = 0; of < expecteds.length; of++) {
            // Localizing.
            let expected = expecteds[of];
            let actual = actuals[of];

            // Actually asserting.  Equality of arrays.
            expect(actual).toEqual(expected);
        }
    });

    it("subtractNodeItem() invokes displayList() after the list is updated.", /* working */ () => {
        /* Arrange. */
        let items = ["One", "Two", "Three"];

        let el = buildItemEl(items[1]);

        let instance = new DomList([...items], null, elItemAbstractor, simpleMatcher);

        let expected = items.length - 1;
        let actual = 0;

        // Target code, overridden to save the test's actual result.
        instance.displayList = () => {
            actual = instance.items.length;
        };

        /* Act. */
        instance.subtractNodeItem(el);

        /* Assert. */
        // Equality of arrays.
        expect(actual).toEqual(expected);
    });

    // endregion subtractNodeItem()

    // region createNodeItemFromStencil()

    it("createNodeItemFromStencil() adds a new item at the end.", /* working */ () => {
        /* Arrange. */
        let items = ["1", "2", "3"];

        // Code that DomList uses to create a new +
        // item from a dedicated fill-in node.
        let starter = () => {
            return "Created";
        };

        let instance = new DomList(
            [...items], null, null,
            null, null, null, starter
        );
        instance.displayList = emptyDisplayList;

        let expected = [...items, "Created"];

        /* Act. */
        instance.createNodeItemFromStencil();

        /* Assert. */
        // Equality of arrays.
        expect(instance.items).toEqual(expected);
    });

    it("createNodeItemFromStencil() provides the instance's .start to its .starter", /* working */ () => {
        /* Arrange. */
        let items = ["1", "2", "3"];

        // A spoof element to pass to starter.
        let start = document.createElement("span");
        start.id = "Start-ID";

        // Code that DomList uses to create a new +
        // item from a dedicated fill-in node.
        let starter = (start) => {
            return start.id;
        };

        // Initing with both start and starter.
        let instance = new DomList(
            [...items], null, null,
            null, null, start, starter
        );
        instance.displayList = emptyDisplayList;

        // If start.id is in .items,
        // it was passed via starter.
        let expected = [...items, start.id];

        /* Act. */
        instance.createNodeItemFromStencil();

        /* Assert. */
        // Equality of arrays.
        expect(instance.items).toEqual(expected);
    });

    it("createNodeItemFromStencil() invokes displayList() after the list is updated.", /* working */ () => {
        /* Arrange. */
        let items = ["1", "2", "3"];

        // Code that DomList uses to create a new +
        // item from a dedicated fill-in node.
        let starter = () => {
            return "Created";
        };

        let instance = new DomList(
            [...items], null, null,
            null, null, null, starter
        );

        let expected = items.length + 1;
        let actual = 0;

        // Target code, overridden to save the test's actual result.
        instance.displayList = () => {
            actual = instance.items.length;
        };

        /* Act. */
        instance.createNodeItemFromStencil();

        /* Assert. */
        // Equality of arrays.
        expect(actual).toEqual(expected);
    });

    // endregion createNodeItemFromStencil()

    // endregion nodes

    // region mixed

    // region addItemAtNode()

    it("addItemAtNode() adds the provided item at the list index for the el provided.", /* working */ () => {
        /* Arrange. */
        let items = ["a", "b", "c", "letter D"];

        // Non-trivial rearrangement for more meaningful testing.
        let ats = [2, 3, 1, 0];
        let reordered = [
            items[ats[0]], items[ats[1]],
            items[ats[2]], items[ats[3]]
        ];
        let els = buildAllItemEls(reordered);

        // Isolated target instances.
        let instances = buildInstances([...items], emptyDisplayList, items.length);

        // Code is exercised on a different instance for each index.
        for (let of = 0; of < instances.length; of++) {
            let instance = instances[of];
            let newItem = `New item ${of}`;

            /* Act. */
            // Adding new el at index from rearranged order.
            instance.addItemAtNode(newItem, els[of]);
        }

        /* Assert. */
        for (let of = 0; of < instances.length; of++) {
            // Localizing.
            let instance = instances[of];
            let at = ats[of];

            // Expected values increment, but the
            // expected indices are reordered.
            let expected = `New item ${of}`;
            let actual = instance.items[at];

            // Actually asserting.
            // The items list should have been lengthened by 1,
            // and the extra value should be the arg to insert.
            expect(instance.items.length).toEqual(items.length + 1);
            expect(actual).toEqual(expected);
        }
    });

    it("addItemAtNode() invokes displayList() after the list is updated.", /* working */ () => {
        /* If displayList() is called, and after the other steps,
           its list length is 1 higher than the original length. */

        /* Arrange. */
        let items = ["a", "b", "c", "d"];

        // Arg elements.
        let atEl = buildItemEl(items[2]);
        let newEl = buildItemEl("New Item");

        // The target of the tests.
        let instance = new DomList([...items], null, elItemAbstractor, simpleMatcher);

        // Defined here because actual is used in nonce lambda.
        let expected = items.length + 1;
        let actual = 0;

        // Target code, overridden to save the test's actual result.
        instance.displayList = () => {
            actual = instance.items.length;
        };

        /* Act. */
        instance.addItemAtNode(newEl, atEl);

        /* Assert. */
        expect(actual).toEqual(expected);
    });

    // endregion addItemAtNode()

    // endregion mixed
});

