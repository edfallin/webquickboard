/**/

export class SpoofDom {
    constructor(doc) {
        let root = document.createElement("body");

        this.doc = root;

        this.blurbSetStencil = doc.getElementById("BlurbSetStencil");
        this.blurbRegionStencil = doc.getElementById("BlurbRegionStencil");
        this.blurbStencil = doc.getElementById("BlurbStencil");

        this.newBlurbSetStencil = doc.getElementById("NewBlurbSetStencil");
        this.newBlurbStencil = doc.getElementById("NewBlurbStencil");

        this.blurbSets = doc.getElementById("BlurbSets");
        this.blurbs = doc.getElementById("Blurbs");
    }
}
