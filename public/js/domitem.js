/**/

export class DomItem {
    constructor(item, site, itemer) /* passed */ {
        this.item = item;
        this.itemer = itemer;
        this.site = site;

        this.index = this.itemer.indexer.index++;
        this.node = this.itemer.builder(this.item, this.index);
    }

    display() /* passed */ {
        this.site.appendChild(this.node);
    }

    equals(other) /* passed */ {
        return this.index === other.index;
    }

    subtract() {
    // ... calls any subtractor, to subtract itself and/or child items or similar ...
    }
}
